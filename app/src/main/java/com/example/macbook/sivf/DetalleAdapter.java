package com.example.macbook.sivf;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class DetalleAdapter extends ArrayAdapter<Detalle> {

        Context mCtx;
        int listLayoutRes;
        List<Detalle> detalleList;
        SQLiteDatabase mDatabase;

        public DetalleAdapter(Context mCtx, int listLayoutRes, List<Detalle> detalleList, SQLiteDatabase mDatabase) {
            super(mCtx, listLayoutRes,detalleList);
            this.mCtx = mCtx;
            this.listLayoutRes = listLayoutRes;
            this.detalleList = detalleList;
            this.mDatabase = mDatabase;
        }

        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(mCtx);
            View view = inflater.inflate(listLayoutRes, null);

            final Detalle detalle = detalleList.get(position);
            //
            TextView textViewProductoNombre = view.findViewById(R.id.textViewProductoNombre);
            TextView textViewProductoPrecio = view.findViewById(R.id.textViewProductoPrecio);
            TextView textViewProductoCantidad = view.findViewById(R.id.textViewProductoCantidad);
            TextView textViewProductoTotal = view.findViewById(R.id.textViewProductoTotal);


            //
            textViewProductoNombre.setText("Producto: "+detalle.getIdProducto());
            textViewProductoPrecio.setText("$"+Double.toString(detalle.getPrecio()));
            textViewProductoCantidad.setText(Integer.toString(detalle.getCantidad()));
            textViewProductoTotal.setText("Total: $"+detalle.getCantidad()*detalle.getPrecio());

            return view;
        }

    }

