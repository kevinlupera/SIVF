package com.example.macbook.sivf;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ClienteActivity extends AppCompatActivity {

    List<Cliente> clienteList;
    SQLiteDatabase mDatabase;
    ListView listViewCliente;
    ClienteAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);

        listViewCliente = (ListView) findViewById(R.id.listViewClientes);
        clienteList = new ArrayList<>();

        //opening the database
        mDatabase = openOrCreateDatabase(AddClienteActivity.DATABASE_NAME, MODE_PRIVATE, null);

        //this method will display the employees in the list
        showClientesFromDatabase();
    }
    private void showClientesFromDatabase() {

            //we used rawQuery(sql, selectionargs) for fetching all the employees
            Cursor cursorClientes = mDatabase.rawQuery("SELECT * FROM cliente", null);

            //if the cursor has some data
            if (cursorClientes.moveToFirst()) {
                //looping through all the records
                do {
                    //pushing each record in the employee list
                    clienteList.add(new Cliente(
                            cursorClientes.getString(0),
                            cursorClientes.getString(1),
                            cursorClientes.getString(2),
                            cursorClientes.getString(3),
                            cursorClientes.getString(4),
                            cursorClientes.getString(5),
                            cursorClientes.getString(6)
                    ));
                } while (cursorClientes.moveToNext());
            }
            //closing the cursor
            cursorClientes.close();

            //creating the adapter object
            adapter = new ClienteAdapter(this, R.layout.list_layout_cliente, clienteList, mDatabase);

            //adding the adapter to listview
            listViewCliente.setAdapter(adapter);
    }
}
