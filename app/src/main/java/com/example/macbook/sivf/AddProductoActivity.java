package com.example.macbook.sivf;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class AddProductoActivity extends AppCompatActivity implements View.OnClickListener {

    EditText editTextProductoNombre, editTextProductoPrecio, editTextProductoCosto,editTextProductoStock;
    TextView textViewVerProductos;
    Spinner  spinnerProductoCategoria;
    Button buttonAddProducto;
    CheckBox checkBoxIVA;

    SQLiteDatabase mDatabase;
    List<Categoria> categoriaList;
    Spinner spinnerCategory;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_producto);

        editTextProductoNombre = findViewById(R.id.editTextProductoNombre);
        editTextProductoPrecio = findViewById(R.id.editTextProductoPrecio);
        editTextProductoStock = findViewById(R.id.editTextProductoStock);
        editTextProductoCosto = findViewById(R.id.editTextProductoCosto);
        spinnerProductoCategoria = findViewById(R.id.spinnerProductoCategoria);
        buttonAddProducto = findViewById(R.id.buttonAddProducto);
        textViewVerProductos = findViewById(R.id.textViewVerProductos);
        checkBoxIVA = findViewById(R.id.checkboxIVA);

        checkBoxIVA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean estado = verificarCheckbox(v);
                if(estado==true){
                    if(!editTextProductoPrecio.getText().toString().isEmpty()) {
                        double precio = Double.parseDouble(editTextProductoPrecio.getText().toString());
                        double precioIva = precio + (precio * MainActivity.IVA);
                        editTextProductoPrecio.setText("" + precioIva);
                        //editTextProductoCosto.setEnabled(false);
                        editTextProductoPrecio.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            }
                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                if(!editTextProductoPrecio.getText().toString().isEmpty()) {
                                    double precio = Double.parseDouble(editTextProductoPrecio.getText().toString());
                                    double costo = precio + (precio * MainActivity.IVA);
                                    editTextProductoPrecio.setText("" + costo);
                                    editTextProductoPrecio.setEnabled(false);
                                }
                                else {
                                }
                            }
                            @Override
                            public void afterTextChanged(Editable s) {
                            }
                        });

                    }else{
                        Toast.makeText(getApplicationContext(),"Ingrese el precio.",Toast.LENGTH_SHORT).show();
                    }
                }else{
                }
            }
        });
        categoriaList = new ArrayList<>();

        //spinnerCategory= findViewById(R.id.spinnerProductoCategoria);
        buttonAddProducto.setOnClickListener(this);
        textViewVerProductos.setOnClickListener(this);

        //opening database
        mDatabase = openOrCreateDatabase(AddClienteActivity.DATABASE_NAME, MODE_PRIVATE, null);
        crearTablaCategoria();
        crearTablaProducto();


        //GET CATEGORY
        getCategoryFromDatabase();
        setSpinner();
    }

    public boolean verificarCheckbox(View v) {
        boolean s = (checkBoxIVA.isChecked() ? true : false);
        return s;
    }


    private void crearTablaCategoria() {
        mDatabase.execSQL(
                "CREATE TABLE IF NOT EXISTS categoria (\n"+
                        "   id_categoria integer NOT NULL CONSTRAINT categoria_pk PRIMARY KEY AUTOINCREMENT,\n"+
                        "   nombre varchar(60) NOT NULL,\n"+
                        "   descripcion varchar(200)\n"+
                        ");"
        );

        mDatabase.execSQL(
                "INSERT INTO categoria (nombre,descripcion) SELECT 'engranajes','son engranajes' WHERE NOT EXISTS(select 1 from categoria where categoria.nombre = 'engranajes');"
        );

        mDatabase.execSQL(
                "INSERT INTO categoria (nombre,descripcion) SELECT 'bombas','son pistones' WHERE NOT EXISTS(select 1 from categoria where categoria.nombre = 'bombas');"
        );

        mDatabase.execSQL(
                "INSERT INTO categoria (nombre,descripcion) SELECT 'sierras','son sierras' WHERE NOT EXISTS(select 1 from categoria where categoria.nombre = 'sierras');"
        );
    }

    private void crearTablaProducto(){
        mDatabase.execSQL(
                "CREATE TABLE IF NOT EXISTS producto (\n"+
                    "   id_producto integer NOT NULL CONSTRAINT producto_pk PRIMARY KEY AUTOINCREMENT,\n" +
                    "   nombre varchar(120) NOT NULL,\n" +
                    "   precio real NOT NULL,\n" +
                    "   costo real NOT NULL,\n" +
                    "   stock interger NOT NULL,\n" +
                    "   categoria interger NOT NULL,\n" +
                    "   FOREIGN KEY(categoria) REFERENCES categoria(id_categoria)\n"+
                    ");"
        );
    }

    private void getCategoryFromDatabase(){
        //we used rawQuery(sql, selectionargs) for fetching all the employees
        Cursor cursorCategoria = mDatabase.rawQuery("SELECT * FROM categoria", null);

        //if the cursor has some data
        if (cursorCategoria.moveToFirst()) {
            //looping through all the records
            do {
                //pushing each record in the employee list
                categoriaList.add(new Categoria(
                        cursorCategoria.getInt(0),
                        cursorCategoria.getString(1),
                        cursorCategoria.getString(2)

                ));
            } while (cursorCategoria.moveToNext());
        }
        //closing the cursor
        cursorCategoria.close();
    }

    public void setSpinner(){
        ArrayList<String> categorias = new ArrayList<>();
        for( Categoria categoria: categoriaList){
            categorias.add(categoria.getNombre());
        }
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(getApplicationContext(),  android.R.layout.simple_spinner_dropdown_item, categorias);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        spinnerProductoCategoria.setAdapter(adapter);
    }

    public boolean validarEntradas(String productoNombre, String productoPrecio, String productoCosto,String productoStock){

        if (productoNombre.isEmpty()){
            editTextProductoNombre.setError("Debe ingresar nombre del producto");
            editTextProductoNombre.requestFocus();
            return false;

        }
        if (productoPrecio.isEmpty() ){
            editTextProductoPrecio.setError("Debe ingresar el precio del producto");
            editTextProductoPrecio.requestFocus();
            return false;
        }

        if(productoPrecio.equals("0")){
            editTextProductoPrecio.setError("El precio no puede ser cero");
            editTextProductoPrecio.requestFocus();
            return false;
        }

        if (productoCosto.isEmpty()){
            editTextProductoCosto.setError("Debe ingresar el costo del producto");
            editTextProductoCosto.requestFocus();
            return false;
        }

        if (productoCosto.equals("0")){
            editTextProductoCosto.setError("El costo no puede ser cero");
            editTextProductoCosto.requestFocus();
            return false;
        }

        if (productoStock.isEmpty()){
            editTextProductoStock.setError("Debe ingresar stock de producto");
            editTextProductoStock.requestFocus();
            return false;
        }

        return true;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonAddProducto:
                //
                addProducto();
                break;
            case R.id.textViewVerProductos:
                //
               Intent intent = new Intent(getApplicationContext(),ProductoActivity.class);
               startActivity(intent);
                break;
        }
    }

    private void addProducto() {
        String productoNombre = editTextProductoNombre.getText().toString().trim();
        String productoPrecio = editTextProductoPrecio.getText().toString().trim();
        String productoCosto = editTextProductoCosto.getText().toString().trim();
        String productoStock = editTextProductoStock.getText().toString().trim();
        String productoCategoria = spinnerProductoCategoria.getSelectedItem().toString();



        if (validarEntradas(productoNombre,productoPrecio,productoCosto,productoStock)){

            double precio = Double.parseDouble(productoPrecio);
            double costo = Double.parseDouble(productoCosto);
            int stock = Integer.parseInt(productoStock);
            int categoria = 0;

            for (Categoria cat: categoriaList) {
                if (cat.getNombre().equals(productoCategoria)){
                    categoria = cat.getIdCategoria();
                }
            }

            String insertSQL = "INSERT INTO producto \n" +
                    "(nombre, precio, costo, stock, categoria)\n" +
                    "VALUES \n"+
                    "(?, ?, ?, ?, ?);";

            if (categoria != 0 ){
                mDatabase.execSQL(insertSQL, new Object[]{productoNombre, precio, costo, stock, categoria});

                Toast.makeText(this, "Producto  Añadido Satisfactoriamente", Toast.LENGTH_SHORT).show();

            }
        }

    }
}
