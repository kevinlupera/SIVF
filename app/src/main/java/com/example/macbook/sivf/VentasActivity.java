package com.example.macbook.sivf;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class VentasActivity extends AppCompatActivity {

    public static List<Detalle> detalleList;
    SQLiteDatabase mDatabase;
    ListView listViewDetalle;
    DetalleAdapter adapter;
    Button buttonAgregarDetalle;
    TextView textViewSubtotal, textViewIVA, textViewTotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ventas);

        buttonAgregarDetalle= findViewById(R.id.buttonAddDetalle);
        listViewDetalle = (ListView) findViewById(R.id.listViewProductos);
        detalleList = new ArrayList<>();
        textViewSubtotal = findViewById(R.id.textViewSubtotal);
        textViewIVA = findViewById(R.id.textViewIVA);
        textViewTotal = findViewById(R.id.textViewTotal);
        //opening the database
        mDatabase = openOrCreateDatabase(AddClienteActivity.DATABASE_NAME, MODE_PRIVATE, null);
        /*imageViewAddProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //this method will display the employees in the list
                AlertDialog.Builder mbuilder = new AlertDialog.Builder(VentasActivity.this);
                View mview = getLayoutInflater().inflate(R.layout.dialog_add_detalle, null);
                final EditText editTextProducto= (EditText) mview.findViewById(R.id.editTextProducto);
                final EditText editTextCantidad= (EditText) mview.findViewById(R.id.editTextProducto);
                final Button buttonAddDetalle = mview.findViewById(R.id.btn_dialog_producto);
                editTextProducto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        AlertDialog.Builder mbuilder = new AlertDialog.Builder(VentasActivity.this);
                        View mview = getLayoutInflater().inflate(R.layout.list_layout_select_producto, null);
                        mbuilder.setView(mview);
                        AlertDialog dialog = mbuilder.create();
                        dialog.show();
                    }
                });

                buttonAddDetalle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                    }
                });
                mbuilder.setView(mview);
                AlertDialog dialog = mbuilder.create();
                dialog.show();



            }
        });*/

        //showProductosFromDatabase();

        detalleList.add(new Detalle(1,1,1,2,4.5));

        adapter = new DetalleAdapter(this, R.layout.list_layout_detalle, detalleList,mDatabase);
        listViewDetalle.setAdapter(adapter);
        buttonAgregarDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(getApplicationContext(),DetalleActivity.class);
                startActivity(intent);
                adapter.notifyDataSetChanged();
                setListViewHeightBasedOnChildren(listViewDetalle);

            }
        });

        totales();

    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.notifyDataSetChanged();
        setListViewHeightBasedOnChildren(listViewDetalle);
        totales();
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }


    public void totales(){

        double subtotal = 0;
        double iva = 0;
        double total = 0;

        if (detalleList != null) {
            for (Detalle detalle : detalleList) {
                subtotal = subtotal + detalle.getPrecio()*detalle.getCantidad();

            }
        }

        iva = subtotal * 0.12;
        total = subtotal + iva;

        textViewSubtotal.setText(Double.toString(subtotal));
        textViewIVA.setText(Double.toString(iva));
        textViewTotal.setText(Double.toString(total));

    }

}
