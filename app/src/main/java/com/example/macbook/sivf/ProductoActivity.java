package com.example.macbook.sivf;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

public class ProductoActivity extends AppCompatActivity {

    List<Producto> productoList;
    List<Categoria> categoriaList;
    SQLiteDatabase mDatabase;
    ListView listViewProducto;
    ProductoAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_producto);

        listViewProducto = (ListView) findViewById(R.id.listViewProductos);
        productoList = new ArrayList<>();
        categoriaList = new ArrayList<>();

        //opening the database
        mDatabase = openOrCreateDatabase(AddClienteActivity.DATABASE_NAME, MODE_PRIVATE, null);

        //this method will display the employees in the list
        showProductosFromDatabase();
    }

    private void showProductosFromDatabase() {

        //we used rawQuery(sql, selectionargs) for fetching all the employees
        Cursor cursorProductos = mDatabase.rawQuery("SELECT * FROM producto", null);

        //obtiene todas las categorias
        showCategoriaFromDatabase();

        //if the cursor has some data
        if (cursorProductos.moveToFirst()) {
            //looping through all the records
            do {
                //pushing each record in the employee list
                productoList.add(new Producto(
                        cursorProductos.getInt(0),
                        cursorProductos.getString(1),
                        cursorProductos.getDouble(2),
                        cursorProductos.getDouble(3),
                        cursorProductos.getInt(4),
                        cursorProductos.getInt(5)
                ));
            } while (cursorProductos.moveToNext());
        }
        //closing the cursor
        cursorProductos.close();

        //creating the adapter object
        adapter = new ProductoAdapter(this, R.layout.list_layout_producto, productoList, categoriaList,mDatabase);

        //adding the adapter to listview
        listViewProducto.setAdapter(adapter);
    }

    private void showCategoriaFromDatabase(){

        Cursor cursorCategorias = mDatabase.rawQuery("SELECT * FROM categoria", null);

        //
        if (cursorCategorias.moveToFirst()){

            do {

                categoriaList.add(new Categoria(
                   cursorCategorias.getInt(0),
                   cursorCategorias.getString(1),
                   cursorCategorias.getString(2)
                ));

            }while (cursorCategorias.moveToNext());
        }

        cursorCategorias.close();

    }
}
