package com.example.macbook.sivf;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class ClienteAdapter extends ArrayAdapter<Cliente> {

    Context mCtx;
    int listLayoutRes;
    List<Cliente> clienteList;
    SQLiteDatabase mDatabase;

    public ClienteAdapter(Context mCtx, int listLayoutRes, List<Cliente> clienteList, SQLiteDatabase mDatabase) {
        super(mCtx, listLayoutRes,clienteList);
        this.mCtx = mCtx;
        this.listLayoutRes = listLayoutRes;
        this.clienteList = clienteList;
        this.mDatabase = mDatabase;
    }

    public View getView(int position,  View convertView,  ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(listLayoutRes, null);

        final Cliente cliente = clienteList.get(position);


        //
        TextView textViewNombreApellido = view.findViewById(R.id.textViewNombreApellido);
        TextView textViewCedula = view.findViewById(R.id.textViewCedula);
        TextView textViewDireccion = view.findViewById(R.id.textViewDireccion);
        TextView textViewTelefono = view.findViewById(R.id.textViewTelefono);

        //
        textViewNombreApellido.setText(cliente.getNombre()+" "+cliente.getApellido());
        textViewCedula.setText(cliente.getCedula());
        textViewDireccion.setText(cliente.getDireccion());
        textViewTelefono.setText(cliente.getTelefono());

        //
        Button buttonEditar = view.findViewById(R.id.buttonEditarCliente);
        Button buttonEliminar = view.findViewById(R.id.buttonEliminarCliente);
        //adding a clicklistener to button
        buttonEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateCliente(cliente);
            }
        });
        //the delete operation
        buttonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                builder.setTitle("¿Estas seguro?");
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String sql = "DELETE FROM cliente WHERE cedula = ?";
                        mDatabase.execSQL(sql, new String[]{cliente.getCedula()});
                        reloadEmployeesFromDatabase();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        return view;


    }
    private void updateCliente(final Cliente cliente) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);

        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.dialog_update_cliente, null);
        builder.setView(view);

        final EditText editTextNombre, editTextApellido, editTextDireccion, editTextFechaNacimiento, editTextTelefono, editTextCorreo;
        editTextNombre = view.findViewById(R.id.editTextNombre);
        editTextApellido = view.findViewById(R.id.editTextApellido);
        editTextDireccion = view.findViewById(R.id.editTextDireccion);
        editTextTelefono = view.findViewById(R.id.editTextTelefono);
        editTextCorreo = view.findViewById(R.id.editTextCorreo);

        editTextNombre.setText(cliente.getNombre());
        editTextApellido.setText(cliente.getApellido());
        editTextDireccion.setText(cliente.getDireccion());
        editTextTelefono.setText(cliente.getTelefono());
        editTextCorreo.setText(cliente.getCorreo());

        final AlertDialog dialog = builder.create();
        dialog.show();

        view.findViewById(R.id.buttonUpdateEmployee).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = editTextNombre.getText().toString().trim();
                String apellido = editTextApellido.getText().toString().trim();
                String direccion = editTextDireccion.getText().toString().trim();
                String telefono = editTextTelefono.getText().toString().trim();
                String correo = editTextCorreo.getText().toString().trim();

                if (nombre.isEmpty()) {
                    editTextNombre.setError("Nombre no puede estar en blanco");
                    editTextNombre.requestFocus();
                    return;
                }

                if (apellido.isEmpty()) {
                    editTextApellido.setError("Apellido no puede estar en blanco");
                    editTextApellido.requestFocus();
                    return;
                }
                if (direccion.isEmpty()) {
                    editTextDireccion.setError("Direccion no puede estar en blanco");
                    editTextDireccion.requestFocus();
                    return;
                }
                if (telefono.isEmpty()) {
                    editTextTelefono.setError("Telefono no puede estar en blanco");
                    editTextTelefono.requestFocus();
                    return;
                }
                if (correo.isEmpty()) {
                    editTextCorreo.setError("Correo no puede estar en blanco");
                    editTextCorreo.requestFocus();
                    return;
                }

                String sql = "UPDATE cliente \n" +
                        "SET nombre = ?, \n" +
                        "apellido = ?, \n" +
                        "direccion = ?, \n" +
                        "telefono = ?, \n" +
                        "correo = ? \n" +
                        "WHERE cedula = ?;\n";

                mDatabase.execSQL(sql, new String[]{nombre, apellido, direccion,telefono ,correo, cliente.getCedula()});
                Toast.makeText(mCtx, "Cliente actualizado", Toast.LENGTH_SHORT).show();
                reloadEmployeesFromDatabase();

                dialog.dismiss();
            }
        });


    }

    private void reloadEmployeesFromDatabase() {
        Cursor cursorClientes = mDatabase.rawQuery("SELECT * FROM cliente", null);
        if (cursorClientes.moveToFirst()) {
            clienteList.clear();
            do {
                clienteList.add(new Cliente(
                        cursorClientes.getString(0),
                        cursorClientes.getString(1),
                        cursorClientes.getString(2),
                        cursorClientes.getString(3),
                        cursorClientes.getString(4),
                        cursorClientes.getString(5),
                        cursorClientes.getString(6)
                        ));
            } while (cursorClientes.moveToNext());
        }
        cursorClientes.close();
        notifyDataSetChanged();
    }
}
