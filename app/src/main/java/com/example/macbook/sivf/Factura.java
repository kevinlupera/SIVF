package com.example.macbook.sivf;

public class Factura {

    private int numFactura;

    private int idCliente;
    private String fecha;


    public Factura() {
    }

    public Factura(int numFactura, int idCliente, String fecha) {
        this.numFactura = numFactura;
        this.idCliente = idCliente;
        this.fecha = fecha;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
}
