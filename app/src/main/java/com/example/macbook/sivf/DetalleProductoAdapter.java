package com.example.macbook.sivf;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DetalleProductoAdapter extends ArrayAdapter<Producto> {

    Context mCtx;
    int listLayoutRes;
    List<Producto> productoList;
    SQLiteDatabase mDatabase;

    public DetalleProductoAdapter(Context mCtx, int listLayoutRes, List<Producto> productoList, SQLiteDatabase mDatabase) {
        super(mCtx, listLayoutRes,productoList);
        this.mCtx = mCtx;
        this.listLayoutRes = listLayoutRes;
        this.productoList = productoList;
        this.mDatabase = mDatabase;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(listLayoutRes, null);

        final com.example.macbook.sivf.Producto producto = productoList.get(position);


        //
        TextView textViewProductoNombre = view.findViewById(R.id.textViewProductoNombre);
        TextView textViewProductoPrecio = view.findViewById(R.id.textViewProductoPrecio);
        TextView textViewProductoStock = view.findViewById(R.id.textViewProductoStock);

        //
        textViewProductoNombre.setText("Producto: "+producto.getNombre());
        textViewProductoPrecio.setText("Precio: $"+Double.toString(producto.getPrecio()));
        textViewProductoStock.setText("Stock: "+Integer.toString(producto.getStock()));
        return view;


    }

    private void reloadProductosFromDatabase() {
        Cursor cursorProductos = mDatabase.rawQuery("SELECT * FROM producto", null);
        if (cursorProductos.moveToFirst()) {
            productoList.clear();
            do {
                productoList.add(new com.example.macbook.sivf.Producto(
                        cursorProductos.getInt(0),
                        cursorProductos.getString(1),
                        cursorProductos.getDouble(2),
                        cursorProductos.getDouble(3),
                        cursorProductos.getInt(4),
                        cursorProductos.getInt(5)
                ));
            } while (cursorProductos.moveToNext());
        }
        cursorProductos.close();
        notifyDataSetChanged();
    }
}
