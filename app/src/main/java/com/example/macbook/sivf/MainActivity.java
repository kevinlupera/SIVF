package com.example.macbook.sivf;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    CardView cvProducto, cvCliente, cvConsulta, cvReporte, cvAjustes, cvVenta;
    public static Double IVA=0.12;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();


        cvProducto= (CardView) findViewById(R.id.productoId);
        cvCliente= (CardView) findViewById(R.id.clienteId);
        cvConsulta= (CardView) findViewById(R.id.consultaId);
        cvReporte= (CardView) findViewById(R.id.reporteId);
        cvAjustes= (CardView) findViewById(R.id.ajusteId);
        cvVenta= (CardView) findViewById(R.id.ventaId);

        cvProducto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AddProductoActivity.class);
                startActivity(i);
            }
        });
        cvCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, AddClienteActivity.class);
                startActivity(i);
            }
        });

        cvVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, VentasActivity.class);
                startActivity(i);
            }
        });
    }
}
