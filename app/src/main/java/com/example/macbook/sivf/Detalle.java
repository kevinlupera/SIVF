package com.example.macbook.sivf;

public class Detalle {
    private int numDetalle;
    private int numFactura;
    private int idProducto;
    private int cantidad;
    private Double precio;

    public Detalle() {
    }

    public Detalle(int numDetalle, int numFactura, int idProducto, int cantidad, Double precio) {
        this.numDetalle = numDetalle;
        this.numFactura = numFactura;
        this.idProducto = idProducto;
        this.cantidad = cantidad;
        this.precio = precio;
    }

    public int getNumDetalle() {
        return numDetalle;
    }

    public void setNumDetalle(int numDetalle) {
        this.numDetalle = numDetalle;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }
}
