package com.example.macbook.sivf;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class DetalleActivity extends AppCompatActivity {

    List<Producto> productoList;
    List<Categoria> categoriaList;
    SQLiteDatabase mDatabase;
    DetalleProductoAdapter adapter;
    Button buttonAceptarDetalle;
    TextView textViewAddDetalle;
    TextView textViewStock;
    ListView listViewProducto;
    EditText editTextCantidad;

    public static Producto producto;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);
        productoList = new ArrayList<>();
        categoriaList = new ArrayList<>();
        //opening the database
        mDatabase = openOrCreateDatabase(AddClienteActivity.DATABASE_NAME, MODE_PRIVATE, null);
        buttonAceptarDetalle=findViewById(R.id.buttonAddDetalleProducto);
        editTextCantidad=findViewById(R.id.editTextCantidad);
        textViewStock = findViewById(R.id.textViewStock);
        textViewAddDetalle = findViewById(R.id.textViewProductoDetalle);
        textViewAddDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(),"asdasd",Toast.LENGTH_SHORT);
                AlertDialog.Builder mbuilder = new AlertDialog.Builder(DetalleActivity.this);
                View mview = getLayoutInflater().inflate(R.layout.dialog_add_detalle, null);
                listViewProducto = (ListView) mview.findViewById(R.id.listViewSelectProducto);
                showProductosFromDatabase();
                mbuilder.setView(mview);
                final AlertDialog dialog = mbuilder.create();
                dialog.show();
                listViewProducto.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        producto = productoList.get(position);

                        textViewAddDetalle.setText(producto.getNombre());
                        textViewStock.setText(""+producto.getStock());

                        dialog.dismiss();

                    }
                });
            }
        });
        buttonAceptarDetalle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (producto!=null){
                    String cant = editTextCantidad.getText().toString().trim();


                    if (cant.isEmpty()){
                        editTextCantidad.setError("Debe ingresar la cantidad");
                        editTextCantidad.requestFocus();
                    }else {
                        int cantidad=Integer.parseInt(cant);

                        if (cantidad>producto.getStock()){
                            editTextCantidad.setError("La cantidad debe ser menor o igual al stock!");
                            editTextCantidad.requestFocus();
                        }else {
                            VentasActivity.detalleList.add(new Detalle(1,1,producto.getId(),cantidad, producto.getPrecio()));
                            finish();
                        }
                    }


                }else{
                    Toast.makeText(getApplicationContext(),"Debe seleccionar un producto",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void showProductosFromDatabase() {

        //we used rawQuery(sql, selectionargs) for fetching all the employees
        Cursor cursorProductos = mDatabase.rawQuery("SELECT * FROM producto", null);

        //obtiene todas las categorias
        showCategoriaFromDatabase();

        //if the cursor has some data
        if (cursorProductos.moveToFirst()) {
            //looping through all the records
            do {
                //pushing each record in the employee list
                productoList.add(new Producto(
                        cursorProductos.getInt(0),
                        cursorProductos.getString(1),
                        cursorProductos.getDouble(2),
                        cursorProductos.getDouble(3),
                        cursorProductos.getInt(4),
                        cursorProductos.getInt(5)
                ));
            } while (cursorProductos.moveToNext());
        }
        //closing the cursor
        cursorProductos.close();

        //creating the adapter object
        adapter = new DetalleProductoAdapter(this, R.layout.list_layout_select_producto, productoList,mDatabase);
        //adding the adapter to listview
        listViewProducto.setAdapter(adapter);
    }

    private void showCategoriaFromDatabase(){

        Cursor cursorCategorias = mDatabase.rawQuery("SELECT * FROM categoria", null);

        //
        if (cursorCategorias.moveToFirst()){

            do {

                categoriaList.add(new Categoria(
                        cursorCategorias.getInt(0),
                        cursorCategorias.getString(1),
                        cursorCategorias.getString(2)
                ));

            }while (cursorCategorias.moveToNext());
        }

        cursorCategorias.close();

    }
}
