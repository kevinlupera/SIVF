package com.example.macbook.sivf;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddClienteActivity extends AppCompatActivity implements View.OnClickListener {

    public static final String DATABASE_NAME = "mibase";

    EditText editTextCedula, editTextNombre, editTextApellido, editTextDireccion, editTextFechaNacimiento, editTextTelefono, editTextCorreo;
    TextView textViewVerClientes;

    SQLiteDatabase mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_cliente);

        editTextCedula = findViewById(R.id.editTextCedula);
        editTextNombre = findViewById(R.id.editTextNombre);
        editTextApellido = findViewById(R.id.editTextApellido);
        editTextDireccion = findViewById(R.id.editTextDireccion);
        editTextFechaNacimiento = findViewById(R.id.editTextFechaNacimiento);
        editTextTelefono = findViewById(R.id.editTextTelefono);
        editTextCorreo = findViewById(R.id.editTextCorreo);

        textViewVerClientes = findViewById(R.id.textViewVerClientes);

        findViewById(R.id.buttonAddCliente).setOnClickListener(this);
        textViewVerClientes.setOnClickListener(this);

        //creando base de datos
        mDatabase = openOrCreateDatabase(DATABASE_NAME, MODE_PRIVATE, null);
        crearTablaCliente();

    }

    private void addCliente(){

        String cedula = editTextCedula.getText().toString().trim();
        String nombre = editTextNombre.getText().toString().trim();
        String apellido = editTextApellido.getText().toString().trim();
        String direccion = editTextDireccion.getText().toString().trim();
        String fechaNacimiento = editTextFechaNacimiento.getText().toString().trim();
        String telefono = editTextTelefono.getText().toString().trim();
        String correo = editTextCorreo.getText().toString().trim();

        if (validarEntradas(cedula, nombre, apellido, telefono)){

            String insertSQL = "INSERT INTO cliente \n" +
                    "(cedula, nombre, apellido, direccion, fecha_nacimiento, telefono, correo)\n" +
                    "VALUES \n" +
                    "(?, ?, ?, ?, ?, ?, ?);";

            mDatabase.execSQL(insertSQL, new String[]{cedula, nombre, apellido, direccion, fechaNacimiento, telefono, correo});

            Toast.makeText(this, "Cliente  Añadido Satisfactoriamente", Toast.LENGTH_SHORT).show();
        }


    }

    private boolean validarEntradas(String cedula, String nombre, String apellido, String telefono){

        if(cedula.isEmpty()){
            editTextCedula.setError("Debe introducir Cedula");
            editTextCedula.requestFocus();
            return false;
        }

        if (nombre.isEmpty()){
            editTextNombre.setError("Debe ingresar Nombre");
            editTextNombre.requestFocus();
            return false;
        }

        if (apellido.isEmpty()){
            editTextApellido.setError("Debe ingresar apellido");
            editTextApellido.requestFocus();
            return false;
        }

        if (telefono.isEmpty()){
            editTextTelefono.setError("Debe ingresar telefono");
            editTextTelefono.requestFocus();
            return false;
        }
        return  true;
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.buttonAddCliente:
                //
                addCliente();
                break;
            case R.id.textViewVerClientes:
                //
                Intent intent = new Intent(getApplicationContext(),ClienteActivity.class);
                startActivity(intent);
                break;
        }

    }

    private void crearTablaCliente() {
        mDatabase.execSQL(
                "CREATE TABLE IF NOT EXISTS cliente (\n" +
                        "    cedula varchar(15) NOT NULL CONSTRAINT cliente_pk PRIMARY KEY,\n" +
                        "    nombre varchar(60) NOT NULL,\n" +
                        "    apellido varchar(60) NOT NULL,\n" +
                        "    direccion varchar(200),\n"+
                        "    fecha_nacimiento text,\n" +
                        "    telefono varchar(10),\n" +
                        "    correo varchar(60)\n" +
                        ");"
        );
    }
}
