package com.example.macbook.sivf;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ProductoAdapter extends ArrayAdapter<Producto> {

    Context mCtx;
    int listLayoutRes;
    List<Producto> productoList;
    List<Categoria> categoriaList;
    SQLiteDatabase mDatabase;

    public ProductoAdapter(Context mCtx, int listLayoutRes, List<Producto> productoList,List<Categoria> categoriaList, SQLiteDatabase mDatabase) {
        super(mCtx, listLayoutRes,productoList);
        this.mCtx = mCtx;
        this.listLayoutRes = listLayoutRes;
        this.productoList = productoList;
        this.categoriaList = categoriaList;
        this.mDatabase = mDatabase;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(listLayoutRes, null);

        final Producto producto = productoList.get(position);


        //
        TextView textViewProductoNombre = view.findViewById(R.id.textViewProductoNombre);
        TextView textViewProductoPrecio = view.findViewById(R.id.textViewProductoPrecio);
        TextView textViewProductoCosto = view.findViewById(R.id.textViewProductoCosto);
        TextView textViewProductoStock = view.findViewById(R.id.textViewProductoStock);
        TextView textViewProductoCategoria = view.findViewById(R.id.textViewProductoCategoria);


        //
        textViewProductoNombre.setText("Producto: "+producto.getNombre());
        textViewProductoPrecio.setText("Precio: "+Double.toString(producto.getPrecio()));
        textViewProductoCosto.setText("Costo: "+Double.toString(producto.getCosto()));
        textViewProductoStock.setText("Stock: "+Integer.toString(producto.getStock()));
        final String categoria = seleccionarCategoria(producto.getCategoria());
        textViewProductoCategoria.setText("Categoria: "+categoria);

        //
        Button buttonEditar = view.findViewById(R.id.buttonEditarProducto);
        Button buttonEliminar = view.findViewById(R.id.buttonEliminarProducto);
        //adding a clicklistener to button
        buttonEditar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProducto(producto, categoria);
            }
        });
        //the delete operation
        buttonEliminar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);
                builder.setTitle("¿Estas seguro?");
                builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String sql = "DELETE FROM producto WHERE id_producto = ?";
                       mDatabase.execSQL(sql, new Object[]{producto.getId()});
                        reloadProductosFromDatabase();
                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }

                });
                AlertDialog dialog = builder.create();
                dialog.show();
            }
        });

        return view;


    }

    private void updateProducto(final Producto producto,String categoria) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mCtx);

        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.dialog_update_producto, null);
        builder.setView(view);

        final EditText editTextProductoNombre, editTextProductoPrecio,editTextProductoCosto, editTextProductoStock;
        final Spinner spinnerProductoCategoria;

        editTextProductoNombre = view.findViewById(R.id.editTextProductoNombre);
        editTextProductoPrecio = view.findViewById(R.id.editTextProductoPrecio);
        editTextProductoCosto = view.findViewById(R.id.editTextProductoCosto);
        editTextProductoStock = view.findViewById(R.id.editTextProductoStock);
        spinnerProductoCategoria = view.findViewById(R.id.spinnerProductoCategoria);


        editTextProductoNombre.setText(producto.getNombre());
        editTextProductoPrecio.setText(Double.toString(producto.getPrecio()));
        editTextProductoCosto.setText(Double.toString(producto.getCosto()));
        editTextProductoStock.setText(Integer.toString(producto.getStock()));
        setSpinner(spinnerProductoCategoria, categoria);


        final AlertDialog dialog = builder.create();
        dialog.show();

        view.findViewById(R.id.buttonUpdateProducto).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nombre = editTextProductoNombre.getText().toString().trim();
                String precio = editTextProductoPrecio.getText().toString().trim();
                String costo = editTextProductoCosto.getText().toString().trim();
                String stock = editTextProductoStock.getText().toString().trim();
                String categoria = spinnerProductoCategoria.getSelectedItem().toString();
                int categ = 0;
                double prec = 0;
                double cost = 0;
                int stck = 0;

                if (nombre.isEmpty()) {
                    editTextProductoNombre.setError("Nombre del producto no puede estar en blanco");
                    editTextProductoNombre.requestFocus();
                    return;
                }

                if (precio.isEmpty()) {
                    editTextProductoPrecio.setError("Precio del producto no puede estar en blanco");
                    editTextProductoPrecio.requestFocus();
                    return;
                }

                if (costo.isEmpty()) {
                    editTextProductoCosto.setError("Costo del producto no puede estar en blanco");
                    editTextProductoCosto.requestFocus();
                    return;
                }
                if (stock.isEmpty()) {
                    editTextProductoStock.setError("Stock del producto no puede estar en blanco");
                    editTextProductoStock.requestFocus();
                    return;
                }


                for (Categoria cat: categoriaList) {
                    if (cat.getNombre().equals(categoria)){
                        categ = cat.getIdCategoria();
                    }
                }

                prec = Double.parseDouble(precio);
                cost = Double.parseDouble(costo);
                stck = Integer.parseInt(stock);

                String sql = "UPDATE producto \n" +
                        "SET nombre = ?, \n" +
                        "precio = ?, \n" +
                        "costo = ?, \n"+
                        "stock = ?, \n" +
                        "categoria = ? \n" +
                        "WHERE id_producto = ?;\n";

                mDatabase.execSQL(sql, new Object[]{nombre, prec, cost, stck, categ, producto.getId()});
                Toast.makeText(mCtx, "Producto actualizado", Toast.LENGTH_SHORT).show();

                reloadProductosFromDatabase();

                dialog.dismiss();
            }
        });


    }

    private String seleccionarCategoria(int categoria){

        String categ;
        for (Categoria cat: categoriaList) {
            if (cat.getIdCategoria() == categoria){
                categ = cat.getNombre();
                return categ;
            }
        }
        return "sin categoria";
    }

    public void setSpinner(Spinner spinnerProductoCategoria, String catego){
        ArrayList<String> categorias = new ArrayList<>();
        for( Categoria categoria: categoriaList){
            categorias.add(categoria.getNombre());
        }
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(mCtx,  android.R.layout.simple_spinner_dropdown_item, categorias);
        adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item);

        spinnerProductoCategoria.setAdapter(adapter);
        spinnerProductoCategoria.setSelection(adapter.getPosition(catego));
    }

    private void reloadProductosFromDatabase() {
        Cursor cursorProductos = mDatabase.rawQuery("SELECT * FROM producto", null);
        if (cursorProductos.moveToFirst()) {
            productoList.clear();
            do {
                productoList.add(new Producto(
                        cursorProductos.getInt(0),
                        cursorProductos.getString(1),
                        cursorProductos.getDouble(2),
                        cursorProductos.getDouble(3),
                        cursorProductos.getInt(4),
                        cursorProductos.getInt(5)
                ));
            } while (cursorProductos.moveToNext());
        }
        cursorProductos.close();
        notifyDataSetChanged();
    }
}
